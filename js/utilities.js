"use strict";
    /**
     * Returns the color we assign to a chosen selector
     * 
     * @param {Object} color 
     * @param {*} selector 
     */
    function changeColor(color,selector){
        selector.style.color = color;
    }
    /**
     * Returns the background color we assign to a chosen selector
     * 
     * @param {Object} color - The color we want to assign
     * @param {Object} selector - The selector that will get the color to its backgroun
     */
    function changeBackground(color,selector){
        selector.style.backgroundColor = color;
    }
    /**
     * Returns the width we assign to a chosen selector
     * 
     * @param {Number} width - The width we will assign
     * @param {Object} selector - The selector that will receive the width
     */
    function changeTagWidth(width,selector){
            selector.style.width = width+'%';
    
    }
    /**
     * Returns the color we assign to a chosen selector
     * 
     * @param {Object} color - The color we want to assign
     * @param {Object} selector - The selector that will receive the color
     */
    function changeBorderColor(color,selector){
        selector.style.borderColor = color;
    }
    /**
     * Returns the width we assign to a chosen selector
     * 
     * @param {Number} width - The width we want to assign
     * @param {Object} selector - The selector that will receive the width
     */
    function changeBorderWidth(width,selector){
            selector.style.borderWidth = width+'px';
   
    }