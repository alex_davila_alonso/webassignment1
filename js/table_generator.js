"use strict";
/**
 * Makes the entire page load before applying anything
 */
document.addEventListener('DOMContentLoaded',function(){
    let col = getColCount();
    let row = getRowCount();
/**
 * Generates the event to get the rows, also removes table everytime so it resets
 */
    generateTable(row,col);
    document.querySelector("#row-count").addEventListener('blur',function(){
        col = getColCount();
        row = getRowCount();
        let oldTable = document.querySelector(".automTbl");
        oldTable.remove();
        let oldText = document.querySelector(".textHtml");
        oldText.remove();
        generateTable(row,col)
    });
/**
 * Generates the event to get the columns, also removes table everytime so it resets
 */
     document.querySelector("#col-count").addEventListener('blur',function(){
        col = getColCount();
        row = getRowCount();
        let oldTable = document.querySelector(".automTbl");
        oldTable.remove();
        let oldText = document.querySelector(".textHtml");
        oldText.remove();
        generateTable(row,col)
    });

});
/**
 * Get the row input value
 * 
 * @returns row value
 */
function getRowCount(){
    let row = document.querySelector("#row-count");
    return row.value;
}
/**
 * Get the column input value
 * 
 * @returns col value
 */
function getColCount(){
    let col = document.querySelector("#col-count");
    return col.value;
}
/**
 * This function will generate a dynamic table, we create all that is necessary for a table
 * then use the function from the utilities to change its rows, columns, colors, and width
 * it also prints the html source code in a textarea below it
 * 
 * @param {Number} rowCount The value we got from the getRowCount function
 * @param {Number} colCount The value we got from the getColCOunt function
 */
function generateTable(rowCount,colCount){
    //Creating table and tbody elements
    let table = document.createElement("table");
    let tbody = document.createElement("tbody");
    //assign ids to variables
    let tabWidth = document.querySelector("#table-width");
    let txtColor = document.querySelector("#text-color");
    let backGroundColor = document.querySelector("#background-color");
    let borderWidth = document.querySelector("#border-width");
    let borderColor = document.querySelector("#border-color");
    /**
     * A loop to fill the table with columns and rows
     */
    for(let i = 0; i < rowCount; i++){
        let row = document.createElement("tr");
        for(let j = 0; j < colCount; j++){
            let col = document.createElement("td");
            col.innerHTML = "cell"+i+j;
            changeBorderColor(borderColor.value,col);
            changeBorderWidth(borderWidth.value,col);
            borderColor.addEventListener('blur',function(){changeBorderColor(borderColor.value,col);});
            borderWidth.addEventListener('blur',function(){changeBorderWidth(borderWidth.value,col);});
            
            row.appendChild(col);
        }
        tbody.appendChild(row);
    }
    table.appendChild(tbody);
    table.setAttribute("class","automTbl");
    /**
     * Calling every function to generate the table with the default values
     */
    changeColor(txtColor.value,table);
    changeBackground(backGroundColor.value,table);
    changeBorderColor(borderColor.value,table);
    changeBorderWidth(borderWidth.value,table);
    changeTagWidth(tabWidth.value,table);

    document.querySelector("#table-render-space").appendChild(table);
    /**
     * Events to change the values of the table 
     */
    tabWidth.addEventListener('blur',function(){changeTagWidth(tabWidth.value,table);});
    txtColor.addEventListener('blur',function(){changeColor(txtColor.value,table);});
    backGroundColor.addEventListener('blur',function(){changeBackground(backGroundColor.value,table);});
    borderWidth.addEventListener('blur',function(){changeBorderWidth(borderWidth.value,table);});
    borderColor.addEventListener('blur',function(){changeBorderColor(borderColor.value,table);});
    
    /**
     * Getting the html source code to display on a textarea
     */
    let htmlSpace = document.querySelector("#table-html-space");
    let txtArea = document.createElement("textarea");
    txtArea.setAttribute("class","textHtml");
    htmlSpace.appendChild(txtArea);
    
    txtArea.innerText = document.querySelector("#table-render-space").innerHTML;


}
